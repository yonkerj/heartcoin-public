// Copyright (c) 2014 Heartcion developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
#include "charitylist.h"
#include "key.h"
#include <boost/foreach.hpp>

static std::vector<CCharity> charities;

struct CCharityData
{
    const char *code;
    const char *label;
    const char *address;
};

static const CCharityData testNetCharityData[] = {
    {"a", "Testnet Charity A", "gfBMoNKuzUDB2KgESUdEkPtJcz29s5mCmK"},
    {"b", "Testnet Charity B", "gdZQc9FvpY8qEWR9i9jXfEJK23zPq3q62Y"},
    {"c", "Testnet Charity C", "gcQPaoWTDcWWdmETqsFX8F4ucyRYGbpyfq"},
    {"d", "Testnet Charity D", "gUEZBQusBGuj6DRXibdXNmdMFri617Tzpu"},
    {"e", "Testnet Charity E", "1JJjf1vQ2uzY9nw68HM6MUuzjvL5miajwo"},
};

bool CCharityList::isValidCharityAddress(const std::vector<unsigned char> &addressData)
{
    if (addressData.size() != 20)
        return false;
    
    uint160 id;
    memcpy(&id, &addressData[0], 20);
    CBitcoinAddress address;
    address.Set(CKeyID(id));
    
    std::vector<CCharity> &charities = getAllCharities();
    BOOST_FOREACH(const CCharity &charity, charities)
    {
        if (charity.address.CompareTo(address) == 0)
        {
            return true;
        }
    }
    return false;
}

const CCharity *CCharityList::getCharityForCode(const std::string &code)
{
    std::vector<CCharity> &charities = getAllCharities();
    BOOST_FOREACH(const CCharity &charity, charities)
    {
        if (charity.code == code)
        {
            return &charity;
        }
    }
    return NULL;
}

std::vector<CCharity> &CCharityList::getAllCharities()
{
    if (charities.empty())
    {
        if (fTestNet)
        {
            for (unsigned int i = 0; i < sizeof(testNetCharityData)/sizeof(CCharityData); i++)
            {
                const CCharityData &data = testNetCharityData[i];
                charities.push_back(CCharity(data.code, data.label, data.address));
            }
        } else {
            
        }
    }
    return charities;
}

