// Copyright (c) 2014 Heartcion developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
#ifndef BITCOIN_CHARITYLIST_H
#define BITCOIN_CHARITYLIST_H

#include <vector>

#include "base58.h"

class CCharity
{
public:
    std::string code;
    std::string label;
    CBitcoinAddress address;
    
    CCharity(const std::string &inCode, const std::string &inLabel, const std::string &inAddress)
    : code(inCode), label(inLabel), address(inAddress) {}
};

class CCharityList
{
public:
    static bool isValidCharityAddress(const std::vector<unsigned char> &addressData);
    static const CCharity *getCharityForCode(const std::string &code);
    static std::vector<CCharity> &getAllCharities();
};

#endif
